import React from "react";
import { createDrawerNavigator } from '@react-navigation/drawer';
import 'react-native-gesture-handler'
import{ AboutUsPage, HomePage, RatingPage, HelpPage} from "../component/Index";

const Drawer = createDrawerNavigator();

export default function Drawerfunc(){
    return(
        <Drawer.Navigator >
            <Drawer.Screen name='History Of Bhutanese Leaders' component={HomePage}/> 
            <Drawer.Screen name='About us' component={AboutUsPage}/>
            <Drawer.Screen name="Rating" component={RatingPage}/>
            <Drawer.Screen name="Help" component={HelpPage}/>
        </Drawer.Navigator>
    )
}