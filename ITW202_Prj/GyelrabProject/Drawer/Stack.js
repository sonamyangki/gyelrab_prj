import {View, Text} from 'react-native'
import React from 'react'
import Drawerfunc from './Drawer'
import { 
    LandingPage, HomePage
} from '../component/Index'
import { createStackNavigator } from '@react-navigation/stack'
import 'react-native-gesture-handler'

const Stack = createStackNavigator();

export default function Stackfunc(){
    return(
        <Stack.Navigator 
          initialRouteName='LandingPage'
          screenOptions={{headerShown: false}}>
          <Stack.Screen name='LandingPage' component={LandingPage}/>
          <Stack.Screen name='HomePage' component={Drawerfunc}/>
        </Stack.Navigator>
    )
}