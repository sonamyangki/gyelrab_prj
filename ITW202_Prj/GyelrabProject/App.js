import { StatusBar } from 'expo-status-bar';
import { View, SafeAreaView , StyleSheet} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import Stackfunc from './Drawer/Stack';

export default function App() {
  return (
      <NavigationContainer>
        <Stackfunc/> 
      </NavigationContainer>
     
  )
}

const styles= StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    
  }
})
