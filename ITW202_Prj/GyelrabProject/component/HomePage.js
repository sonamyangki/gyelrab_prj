import React from "react";
import { ImageBackground, StyleSheet, Text, View } from "react-native";
import { Button } from "react-native-paper";

export default function HomePage(){
    return(
      <View style= {styles.container}>
      <ImageBackground source={require('../assets/bgimage.jpg')} resizeMode="cover" style={styles.image}>
      </ImageBackground>
           {/* <Button
            mode="contained"
            onPress={()=>navigation.replace('HomePage')}>Let's Go!!!</Button>  */}

    </View>
  )
}



const styles= StyleSheet.create({
  container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
  },
    image: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      height: '100%',
      width: '100%',
      position: 'absolute'
    },
})
