import React from "react";
import { ImageBackground, StyleSheet, Text, View } from "react-native";
import { Button } from "react-native-paper";

export default function AboutUsPage(){
    return(
        <View style= {styles.container}>
            <Text>About us Page</Text>
        </View>
  )
}



const styles= StyleSheet.create({
  container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
  },
    image: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      height: '100%',
      width: '100%',
      position: 'absolute'
    },
})
