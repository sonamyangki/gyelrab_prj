import React,{Component} from 'react';
import StarRating from 'react-native-star-rating';
import {View, Text, StyleSheet, ImageBackground,} from 'react-native'
import {Button} from 'react-native-paper'
 
class GeneralStarExample extends Component {
 
  constructor(props) {
    super(props);
    this.state = {
      starCount: 0
    };
  }
 
  onStarRatingPress(rating) {
    this.setState({
      starCount: rating
    });
  }
 
  render() {
    return (
      <View style= {styles.container}>
        
            <ImageBackground source={require('../assets/bgimage.jpg')} resizeMode="cover" style={styles.image}>
              <View style={styles.rating}>
              <Text style={styles.text}>RATING(SELECT A STAR TO RATE OUR APP)</Text>
                <StarRating
                  disabled={false}
                  maxStars={5}
                  rating={this.state.starCount}
                  selectedStar={(rating) => this.onStarRatingPress(rating)}
                />
                <Button 
                  style={styles.button}
                  onPress={()=>{alert("Rating saved!!!")}}
                  mode='outlined'
                  >
                  SEND
                </Button>
              </View>
            </ImageBackground>
            
        </View>
      
    );
  }
}
 
const styles= StyleSheet.create({
  container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
  },
  image: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    width: '100%',
    position: 'absolute'
  },
  rating: {
    backgroundColor: '#E2EAE6'
  },
  text: {
    fontSize: 15,
    padding: 20,
    fontWeight: 'bold'
  },
  button: {
    padding: 10,
    
  }
  
})

export default GeneralStarExample
